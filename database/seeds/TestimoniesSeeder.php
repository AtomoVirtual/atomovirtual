<?php

use Illuminate\Database\Seeder;
use App\Models\Testimony;

class TestimoniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Testimony::create([
            'content'       => 'Recomiendo ampliamente a mis Bros de Àtomo Virtual, full activos para cualquier idea y responsables en cada aspecto. Equipo creativo e inteligente.',
            'client_name'   => 'Emile Trader',
            'client_pic' => 'assets_web/img/testmonial/profit.jpeg',
            'client_carreer' => 'CEO/Fundador Profit 4 Life'
        ]);
        Testimony::create([
            'content'       => 'Ha sido una experiencia increíble trabajar con este equipo, muy responsables y profesionales en todo sentido. Me sorprendió la velocidad y calidad del proyecto realizado junto a sus tiempos de soporte para que mi plataforma siempre este al 100%.',
            'client_name'   => 'Anderson Urbano',
            'client_pic' => 'assets_web/img/testmonial/cerv.jpeg',
            'client_carreer' => 'CEO/Fundador Estación Cervecera'
        ]);
    }
}
