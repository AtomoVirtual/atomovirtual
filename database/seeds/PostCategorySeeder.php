<?php

use Illuminate\Database\Seeder;
use App\Models\PostsCategory;

class PostsCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostsCategory::create([
            'title' => 'HTML'
        ]);
        PostsCategory::create([
            'title' => 'CSS'
        ]);
        PostsCategory::create([
            'title' => 'PHP'
        ]);
    }
}
