<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
            'title' => 'POST HTML',
            'content' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolores sed, atque blanditiis maxime vitae dignissimos ipsa expedita earum voluptatum accusamus sit! Rem itaque consequatur blanditiis, rerum possimus quam similique.',
            'img_url' => 'assets_web/img/blog/single_blog_1.png',
            'postcategory_id' => '1'
        ]);
        Post::create([
            'title' => 'POST FRAMEWORKS',
            'content' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolores sed, atque blanditiis maxime vitae dignissimos ipsa expedita earum voluptatum accusamus sit! Rem itaque consequatur blanditiis, rerum possimus quam similique.',
            'img_url' => 'assets_web/img/blog/single_blog_1.png',
            'postcategory_id' => '1'
        ]);
        Post::create([
            'title' => 'POST LARAVEL',
            'content' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolores sed, atque blanditiis maxime vitae dignissimos ipsa expedita earum voluptatum accusamus sit! Rem itaque consequatur blanditiis, rerum possimus quam similique.',
            'img_url' => 'assets_web/img/blog/single_blog_1.png',
            'postcategory_id' => '1'
        ]);
        Post::create([
            'title' => 'POST PHP',
            'content' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima dolores sed, atque blanditiis maxime vitae dignissimos ipsa expedita earum voluptatum accusamus sit! Rem itaque consequatur blanditiis, rerum possimus quam similique.',
            'img_url' => 'assets_web/img/blog/single_blog_1.png',
            'postcategory_id' => '1'
        ]);
    }
}
