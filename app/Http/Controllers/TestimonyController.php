<?php

namespace App\Http\Controllers;

use App\Models\Testimony;

use Illuminate\Http\Request;

use DB;

class TestimonyController extends Controller
{

    public function index()
    {
        $testimonies = Testimony::all();

        return view('admin.testimonies.index', compact('testimonies'));
    }

    public function destroy($id)
    {
        DB::table("testimonies")->where('id',$id)->delete();
        return redirect()->route('admin.testimonies')
                        ->with('success','Testimonio Eliminado con Éxito');
    }

}
