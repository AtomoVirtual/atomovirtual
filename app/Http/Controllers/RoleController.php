<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{

    public function index()
    {
        $roles = Role::all();

        return view('admin.roles.index', compact('roles'));
    }

    public function destroy($id)
    {
        DB::table("users")->where('id',$id)->delete();
        return redirect()->route('admin.users')
                        ->with('success','Usuario Eliminado con Éxito');
    }

}
