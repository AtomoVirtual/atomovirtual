<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;

use DB;

class BlogController extends Controller
{

    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    public function destroy($id)
    {
        DB::table("users")->where('id',$id)->delete();
        return redirect()->route('admin.users')
                        ->with('success','Usuario Eliminado con Éxito');
    }

}
