<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use DB;
use Illuminate\Support\Facades\Input;
use Storage;
use File;
use Crypt;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = Contact::all()->sortByDesc('id');
        return view('admin.contact.index',compact('contacts'));
    }


    public function store(Request $request)
    {

        if($request->all()['hidden_email'] == "1"){

            $this->validate($request, [
                'email' => 'required|email',
            ]);

            DB::beginTransaction();

            $contact = New Contact();
            $contact->email = $request->email;
            $contact->save();

            if (!$contact) {

                DB::rollBack();
                return redirect()->back()->with('error', 'error');
            }

            DB::commit();
            return redirect()->back()->with('success', 'success');
        }
        else{

            $this->validate($request, [
                'name' => 'required',
                'lastname' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'message' => 'required',
                'subject' => 'required',
            ]);

            DB::beginTransaction();

            $contact = New Contact();
            $contact->name = $request->name;
            $contact->lastname = $request->lastname;
            $contact->email = $request->email;
            $contact->phone = $request->phone;
            $contact->message = $request->message;
            $contact->subject = $request->subject;
            $contact->save();

            if (!$contact) {

                DB::rollBack();
                return redirect()->back()->with('error', 'error');
            }

            DB::commit();
            return redirect()->back()->with('success', 'success');

        }
    }



    public function destroy(Request $request, $id)
    {
        DB::table("contacts")->where('id',$id)->delete();
        return redirect()->back()->with('success', 'success');
    }
}
