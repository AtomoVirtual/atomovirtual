<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PostsCategory;
use App\Models\Post;
use App\Models\Testimony;
use DB;

class PageController extends Controller
{

    public function index(Request $request)
    {
        $testimonies = Testimony::all();

        if(key($request->all())){
            $posts = DB::table('posts')
            ->leftjoin('posts_categories', 'posts.postcategory_id', '=', 'posts_categories.id')
            ->where('posts_categories.id', '=', key($request->all()))
            ->select('posts_categories.title as category_title', 'posts.*')
            ->get();
        }
        else{
            $posts = DB::table('posts')
            ->leftjoin('posts_categories', 'posts.postcategory_id', '=', 'posts_categories.id')
            ->select('posts_categories.title as category_title', 'posts.*')
            ->get();
        }

        return view('welcome', compact('posts', 'testimonies'));
    }

    public function about()
    {
        return view('about');
    }

    public function services()
    {
        return view('services');
    }
    public function contact()
    {
        return view('contact');
    }
    public function terms()
    {
        return view('terms');
    }
    public function policy()
    {
        return view('policy');
    }
    public function cookies()
    {
        return view('cookies');
    }
    public function blog(Request $request)
    {

            $posts = DB::table('posts')
            ->leftjoin('posts_categories', 'posts.postcategory_id', '=', 'posts_categories.id')
            ->select('posts_categories.title as category_title', 'posts.*')
            ->paginate(2);



        $categories = PostsCategory::all();
        $posts_randoms = Post::inRandomOrder()->limit(3)->get();
        return view('blog', compact('categories', 'posts','posts_randoms'));
    }


    public function blog_cat(Request $request, $id)
    {

            $posts = DB::table('posts')
            ->leftjoin('posts_categories', 'posts.postcategory_id', '=', 'posts_categories.id')
            ->where('posts_categories.id', '=', $id)
            ->select('posts_categories.title as category_title', 'posts.*')
            ->paginate(2);




        $categories = PostsCategory::all();
        $posts_randoms = Post::inRandomOrder()->limit(3)->get();
        return view('blog', compact('categories', 'posts','posts_randoms'));
    }








    public function post(Request $request, $id_post)
    {

        $post = DB::table('posts')
			->join('posts_categories', 'posts.postcategory_id', '=', 'posts_categories.id')
            ->where('posts.id', $id_post)
            ->select('posts.*', 'posts_categories.title as cat_title')
			->first();
        $posts = DB::table('posts')
            ->leftjoin('posts_categories', 'posts.postcategory_id', '=', 'posts_categories.id')
            ->select('posts_categories.title as category_title', 'posts.*')
            ->get();

        $categories = PostsCategory::all();
        $posts_randoms = Post::inRandomOrder()->limit(3)->get();
        return view('post', compact('categories','posts_randoms', 'post', 'posts'));
    }

}
