<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['register' => false]);


//Rutas Vistas Sitio Web

Route::get('/', ['as' => 'home.index', 'uses' => 'PageController@index']);

Route::get('acerca-de-nosotros', ['as' => 'home.about', 'uses' => 'PageController@about']);

Route::get('nuestros-servicios', ['as' => 'home.services', 'uses' => 'PageController@services']);

Route::get('contactanos', ['as' => 'home.contact', 'uses' => 'PageController@contact']);
Route::post('contactanos', ['as'   => 'home.contact.store', 'uses'   => 'ContactController@store']);


Route::get('terminos-y-condiciones', ['as' => 'home.terms', 'uses' => 'PageController@terms']);

Route::get('politica-de-privacidad', ['as' => 'home.policy', 'uses' => 'PageController@policy']);

Route::get('politica-de-cookies', ['as' => 'home.cookies', 'uses' => 'PageController@cookies']);

Route::get('blog/', ['as' => 'home.blog', 'uses' => 'PageController@blog']);

Route::get('blog/{id}', ['as' => 'home.blog_cat', 'uses' => 'PageController@blog_cat']);

Route::get('post/{id_post}', ['as' => 'home.post', 'uses' => 'PageController@post']);


//End Rutas Vistas Sitio Web


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin/roles', ['as' => 'admin.roles', 'uses' => 'RoleController@index']);
    Route::get('admin/users', ['as' => 'admin.users', 'uses' => 'UserController@index']);
    Route::get('admin/users/delete/{id}', ['as' => 'admin.users.destroy', 'uses'   => 'UserController@destroy']);
    Route::get('admin/contact/index', ['as'      => 'admin.contact.index', 'uses'           => 'ContactController@index']);
    Route::get('admin/contact/delete/{id}', ['as' => 'admin.contact.destroy', 'uses'   => 'ContactController@destroy']);
});
