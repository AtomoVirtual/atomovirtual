<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

    <main>

        <!-- Slider Area Start-->
        <div class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center"
                    data-background="{{URL::asset('assets_web/img/hero/h1_hero.png')}}">
                    <div class="container">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-7 col-md-9 ">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay=".4s">Creamos Experiencias<br> Web Increibles
                                    </h1>
                                    <p data-animation="fadeInLeft" data-delay=".6s" class="pr-120">Te ayudamos a crear una presencia solida en internet y que consigas el éxito con tu proyecto.</p>
                                    <!-- Hero-btn -->
                                    <form class="form-contact" action="{{route('home.contact.store')}}" method="POST"  >
                                        {{ csrf_field() }}
                                        <div class="mt-1">
                                            <input type="email" name="email" placeholder="Deja tu email y empecemos a crear" onfocus="this.placeholder = ''"
                                                onblur="this.placeholder = 'Email'" required class="single-input col-md-8">
                                        </div>
                                        <div class="hero__btn mt-3" data-animation="fadeInLeft" data-delay=".8s">
                                            <button type="submit" class="button btn hero-btn">Trabajemos Juntos</button>
                                        </div>
                                        <input type="hidden" name="hidden_email" value="1">
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="hero__img d-none d-lg-block" data-animation="fadeInRight" data-delay="1s">
                                    <img src="{{URL::asset('assets_web/img/hero/hero_right.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Area End-->
        <!-- What We do start-->
        <div class="what-we-do we-padding">
            <div class="container">
                <!-- Section-tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <h2>Nuestros servicios principales</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-10">
                            <div class="do-icon">
                                <span class="flaticon-null"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Desarrollo Móvil</h4>
                                <p>Nos enfocamos en crear aplicaciones móviles diseñadas a la medida, cuéntanos que necesitas y diseñaremos el mejor producto para superar tus expectativas.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero una App</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-null-1"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Desarrollo Web</h4>
                                <p>Innovamos con los mejores diseños para impactar a tus clientes, cada proyecto es único, personalizado y optimizado para posicionar en los buscadores.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero una Web</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-null-3"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Cursos Online</h4>
                                <p>Nos gusta compartir nuestros conocimientos y ver como los demás tienen éxito, si quieres aprender a crear aplicaciones móviles y sitios web contáctanos.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero Aprender</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- What We do End-->
        <!-- We Create Start -->
        <div class="we-create-area create-padding">
            <div class="container">
                <div class="row d-flex align-items-end">
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-img mb-30">
                            <img src="{{URL::asset('assets_web/img//service/we-create.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-cap">
                            <h3>Motívate y vuelve realidad tu proyecto.</h3>
                            <p>Lo mejor que puedes hacer es invertir en ti como producto personal o en ese proyecto que tienes en mente, no te detengas y siempre persigue el éxito.</p>
                            <a href="{{route('home.contact')}}" class="btn">Contáctanos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- We Create End -->
        <!-- Generating Start -->
        <div class="generating-area">
            <div class="container">
                <!-- Section-tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <h2>¿Como te ayudamos a crecer?​</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-null-2"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Posicionándote en la Web</h4>
                                <p>Haremos que tu marca o producto sea potente en los buscadores y así atraer muchos mas clientes. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-null-4"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Creación de Canales</h4>
                                <p>Creamos tus perfiles en las redes sociales que necesites y los optimizamos para que sean impactantes. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-crecimiento"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Analítica Web</h4>
                                <p>Conectamos tu web con la herramienta de Google Analytics, así podrás ver el impacto del proyecto. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-soporte-tecnico"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Servicio de Soporte</h4>
                                <p>Disponemos de servicio de soporte y mantenimiento para todos nuestros proyectos a la medida. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Generating End -->
        <!-- What We do start-->
        <div class="what-we-do choose-padding">
            <div class="container">
                <!-- Section-tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <h2>Forma parte de Átomo Virtual</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-10">
                            <div class="do-icon">
                                <span class="flaticon-avion"></span>
                            </div>
                            <div class="do-caption">
                                <h4>¿Quieres ser parte de en nuestro foro?</h4>
                                <p>Si necesitas preguntar cualquier duda que tengas y que la comunidad te solucione el problema que tengas puedes a nuestro foro y buscar esas respuestas que tanto quieres.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero Opinar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-avion-1"></span>
                            </div>
                            <div class="do-caption">
                                <h4>¿Quieres respuestas de un profesional?</h4>
                                <p>Contamos con un grupo especializado para responder cualquier duda importante o que requieran asesoramiento directo, trabajaremos para resolver cualquier problema.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero Asesorías</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-proyecto"></span>
                            </div>
                            <div class="do-caption">
                                <h4>¿Quieres trabajar en nuestro equipo?</h4>
                                <p>Estas desempleado o simplemente quieres buscar nuevas oportunidades de trabajo, postúlate para formar parte de nuestro equipo en Átomo Virtual, te estamos esperando.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero Trabajar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- What We do End-->
        <!-- Visit Stuffs Start -->
        <div class="visit-area fix choose-padding">
            <div class="container">
                <!-- Section-tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6 pr-0">
                        <div class="section-tittle text-center">
                            <h2>Últimos Proyectos Realizados</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-0">
                <div class="row ">
                    <div class="col-md-6">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img class="rounded" src="{{URL::asset('assets_web/img/visit/profit.jpg')}}" alt="">
                            </div>
                            <div class="visited-cap">
                                <h3><a href="https://profit4lifevnzla.com/">Profit 4 Life</a></h3>
                                <p>Plataforma de Cursos Forex</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img class="rounded" src="{{URL::asset('assets_web/img/visit/esteacion-cervecera.jpg')}}" alt="">
                            </div>
                            <div class="visited-cap">
                                <h3><a href="http://estacioncervecera.com/">Estación Cervecera</a></h3>
                                <p>Tienda Online Cervezas</p>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-4">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img class="rounded" src="{{URL::asset('assets_web/img/visit/medico.jpg')}}" alt="">
                            </div>
                            <div class="visited-cap">
                                <h3><a href="https://mimedico.com/">Mi Médico</a></h3>
                                <p>Plataforma de citas Médicas</p>
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
        <!-- Visit Stuffs Start -->
        <!-- Testimonial Start -->
        <div class="testimonial-area tips-padding">
            <div class="container">
                <div class="testimonial-main">
                    <!-- Section-tittle -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-5  col-md-8 pr-0">
                            <div class="section-tittle text-center">
                                <h2>Opiniones de Nuestros Clientes</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-10 col-md-9">
                            <div class="h1-testimonial-active">
                                <!-- Single Testimonial -->

                                @foreach($testimonies as $test)
                                <div class="single-testimonial text-center">
                                    <div class="testimonial-caption ">
                                        <div class="testimonial-top-cap">
                                            <p>{{$test->content}} </p>
                                        </div>
                                        <!-- founder -->
                                        <div
                                            class="testimonial-founder d-flex align-items-center justify-content-center">
                                            <div class="founder-img">
                                                <img src="{{URL::asset($test->client_pic)}}" alt="">
                                            </div>
                                            <div class="founder-text">
                                                <span>{{$test->client_name}}</span>
                                                <p>{{$test->client_carreer}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- Single Testimonial -->
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </br>
        </br>
        </div>
        <!-- Testimonial End -->
        <!-- Tips Triks Start
        <div class="tips-triks-area tips-padding">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6 col-md-8 pr-0">
                        <div class="section-tittle text-center">
                            <h2>Blog de Trucos y Contenido Interesante</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($posts as $post)
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-tips mb-50">
                            <div class="tips-img">
                                <a href="{{route('home.post', $post->id)}}"><img src="{{URL::asset('assets_web/img/tips/tips_1.jpg')}}" alt=""></a>
                            </div>
                            <div class="tips-caption">
                                <h4><a href="{{route('home.post', $post->id)}}">Crear estructuras HTML optimizadas</a></h4>
                                <a href="{{route('home.post', $post->id)}}"><span>Continuar Leyendo</span></a>
                                <p>Enero 3, 2020</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        -->
        <!-- Tips Triks End -->
        @include('layouts.cta')

    </main>


@include('layouts.footer')

@include('layouts.scripts')
@if(session()->has('flashModal'))
<script>
  $('#SuccessModal').modal('show');
</script>
@endif


</body>

</html>
