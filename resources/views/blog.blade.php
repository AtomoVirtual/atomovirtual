<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<main>
   <!-- Slider Area Start-->
    <div class="services-area">
        <div class="container">
            <!-- Section-tittle -->
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="section-tittle text-center mb-80">
                        <span>Blog</span>
                        <h2>Nuestra Área de Blog</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Area End-->

    <!--================Blog Area =================-->
    <section class="blog_area section-paddingr">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                    @foreach($posts as $post)
                        <article class="blog_item">
                            <div class="blog_item_img">
                                <img class="card-img rounded-0" src="{{URL::asset($post->img_url)}}" alt="">
                                <a href="{{route('home.post', $post->id )}}" class="blog_item_date">
                                    <h3>03</h3>
                                    <p>Ene</p>
                                </a>
                            </div>

                            <div class="blog_details">
                                <a class="d-inline-block" href="{{route('home.post', $post->id )}}">
                                    <h2>{{$post->title}}</h2>
                                </a>
                                <p>{{$post->content}}</p>
                                <ul class="blog-info-link">
                                    <li><a href="{{route('home.post', $post->id )}}"><i class="fa fa-user"></i> {{$post->category_title}}</a></li>
                                </ul>
                            </div>
                        </article>
                    @endforeach
                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                <li class="page-item">
                                    {{ $posts->links() }}
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('layouts.blog_sidebar')
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->


   @include('layouts.cta')
</main>

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
