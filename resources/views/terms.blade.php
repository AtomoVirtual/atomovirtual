<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<main>
     <!-- Slider Area Start-->
       <div class="services-area">
         <div class="container">

             <!-- Section-tittle -->
             <div class="row d-flex justify-content-center">
                 <div class="col-lg-8">
                     <div class="section-tittle text-center mb-80">
                         <span>Aviso Legal</span>
                         <h2>Términos y Condiciones​</h2>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- Slider Area End-->
     
   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 posts-list">
               <div class="single-post">
                  <div class="blog_details">
                     <h2>Información legal</h2>
                     <p class="excert">
                        El artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio
                        Electrónico establece la obligación de todos los prestadores de servicios de la sociedad de la información de disponer
                        de medios que permitan, tanto a los destinatarios del servicio como a los órganos competentes, a acceder por medios
                        electrónicos, de forma permanente, fácil, directa y gratuita a determinada información que proporcionamos en este
                        apartado.
                     </p>
                  </div>
                  <div class="blog_details">
                     <h2>Condiciones de uso</h2>
                     <p class="excert">
                        Al usar el sitio web, se entiende que el usuario ha leído, entendido y aceptado íntegramente y sin reservas este aviso legal, las condiciones de uso, la política de privacidad y demás avisos o instrucciones que figuran en este sitio web, comprometiéndose a hacer un buen uso del mismo conforme a la ley, la moral y el orden público. En caso de que el usuario no esté de acuerdo, deberá abstenerse de utilizar el sitio web <a href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a>
                     </p>
                     
                     <p>
                        Átomo Virtual se reserva el derecho de modificar en cualquier momento los contenidos, informaciones, los términos de este sitio web, de las presentes condiciones de uso, políticas de privacidad y demás avisos legales. En este caso, informaremos a los usuarios a través del sitio web o enviando un correo electrónico informativo a quien nos lo solicite al objeto de que puedan decidir si desean continuar utilizando el sitio web.
                     </p>
                     <p>
                        En el sitio web <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> pueden existir enlaces a otros sitios o páginas web de terceros si bien no asume ningún tipo de responsabilidad sobre ellos ya que no tiene ningún tipo de control sobre los mismos. Por ello, el usuario accede bajo su exclusiva responsabilidad tanto al contenido que ofrecen como a sus condiciones de uso.
                     </p>
                  </div>
                  <div class="blog_details">
                     <h2>Derechos de autor y propiedad intelectual</h2>
                     <p class="excert">
                        El usuario reconoce y acepta que todas las marcas, nombres comerciales o signos distintivos, contenidos, todos los
                        derechos de propiedad industrial e intelectual, y/o cualesquiera otros elementos insertados en este sitio web, son
                        propiedad exclusiva de Átomo Virtual y/o de terceros, quienes tiene el derecho exclusivo de utilizarlos en el tráfico económico.
                     </p>
                     <p>
                        En ningún caso el acceso a la página web implica ningún tipo de renuncia, transmisión, licencia o cesión total ni
                        parcial de dichos derechos, salvo que se establezca expresamente lo contrario.
                     </p>
                     <p>
                        De acuerdo con lo expuesto en el párrafo anterior, los usuarios de este sitio web tienen prohibida su reproducción,
                        copia, cesión, distribución, modificación o cualquier otro uso, de forma total o parcial, respecto de la información y
                        contenido de este sitio web sin la autorización previa y por escrita de Átomo Virtual.
                     </p>
                     <p>
                        El contenido de este sitio web no podrá ser reproducido ni en todo ni en parte, ni transmitido, ni registrado por ningún sistema de recuperación de información, en ninguna forma ni en ningún medio, a menos que se cuente con la autorización previa, por escrito, de la citada Entidad.
                     </p>
                     <p>
                        Átomo Virtual es titular de los elementos que integran el diseño gráfico de su sitio web, los menús, el código HTML, los textos, marcas, logotipos, combinaciones de colores, botones, imágenes, gráficos y cualquier otro contenido del sitio web, así como de la estructura, selección, ordenación y presentación de sus contenidos o, en cualquier caso dispone de la correspondiente autorización para la utilización de dichos elementos.
                     </p>                     
                     <p>
                        Por tanto, el usuario se obliga a usar este sitio web, sus contenidos y servicios de forma diligente y correcta, de
                        acuerdo a la Ley, la moral, los buenos usos y costumbres, al orden público, la buena fe y estas condiciones generales de uso, con escrupuloso respeto a los derechos de propiedad intelectual que corresponden a Átomo Virtual.
                     </p>
                     <p>
                        Se prohíbe expresamente al usuario que utilice el sitio web con fines ilícitos, prohibidos, lesivos de derechos de
                        terceros o que puedan, de cualquier forma, dañar la marca, la imagen o reputación de Átomo Virtual.
                     </p>
                     <p>
                        El usuario responderá de los daños y perjuicios de cualquier clase que Átomo Virtual pueda sufrir como consecuencia del
                        incumplimiento de cualquiera de las obligaciones a las que queda sometido por estas condiciones que sean de aplicación.
                     </p>
                     <p>En este sentido, Átomo Virtual se reserva el ejercicio de las acciones legales oportunas en defensa de sus derechos.</p>
                  </div>
                  <div class="blog_details">
                     <h2>Protección de Datos</h2>
                     <p class="excert">
                     En el sitio web <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> se recaban datos personales a través de varias vías, ya sea mediante el formulario existente o a mediante la dirección de correo electrónico habilitada para establecer contacto con nosotros.
                     </p>

                     <p>
                        Tales datos serán tratados respetando la normativa vigente en cada momento en materia de protección de datos de carácter
                        personal, y de acuerdo con la política de privacidad descrita en el presente sitio web, que los usuarios deberán leer
                        antes de facilitar sus datos.
                     </p>
                     <p>
                        En todo caso, en cada formulario utilizado para recabar datos de carácter personal se ha habilitado una cláusula
                        informativa sobre el tratamiento que se hace de los mismos en cada uno de los formularios.
                     </p>
                  </div>
                  <div class="blog_details">
                     <h2>Comunicaciones comerciales</h2>
                     <p>
                        De acuerdo con lo dispuesto por la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de
                        Comercio Electrónico, se informa a los usuarios del sitio web <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> que no se les enviará publicidad o
                        comunicaciones comerciales por correo electrónico u otros medios de comunicación electrónica equivalentes sin su previa solicitud o consentimiento, que será obtenido a través de los correspondientes formularios habilitados al efecto.
                     </p>
                     <p>
                        En todo caso los usuarios pondrán oponerse o manifestar su negativa a la recepción de información comercial por medios
                        electrónicos enviando un correo a contacto@atomotecnovirtual.com , facilitándose esta posibilidad en cada comunicación que se le envíe.
                     </p>
                  </div>
                  
                  <div class="blog_details">
                     <h2>Responsabilidad del titular del sitio web</h2>
                     
                     <p>
                        Átomo Virtual no garantiza la ausencia de virus ni de otros elementos en los contenidos, por lo que no asume ninguna
                        responsabilidad por daños que se puedan causar en los equipos o sistemas de los usuarios por posibles virus informáticos
                        que se hubieran podido contraer por la navegación del usuario en el sitio web.
                     </p>
                     
                     <p>
                        Del mismo modo, el titular del sitio web <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> tampoco controla ni garantiza el acceso continuado, ni la correcta
                        visualización, descarga o utilidad de los elementos e informaciones contenidas en su sitio web, que pueden verse
                        impedidos, dificultados o interrumpidos por factores o circunstancias que están fuera de su control y no se hace
                        responsable en caso de existir interrupciones del servicio, demoras o mal funcionamiento cuando se deban a causas ajenas
                        al control de Átomo Virtual, por fuerza mayor, ni la que se deba a una actuación dolosa o culposa por parte del usuario.
                     </p>
                     
                     <p>
                        Átomo Virtual no asume ninguna responsabilidad por los contenidos y opiniones vertidos por los usuarios del sitio web
                        <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> ni de la información de sitios web de terceros a las que se puede acceder a través de buscadores o enlaces
                        a <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a>.
                     </p>
                  </div>
                  
                  <div class="blog_details">
                     <h2>Uso de la web por menores de edad</h2>
                     
                     <p>
                        Aunque el sitio web <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> no está dirigido a menores de edad, se les permite su acceso. No obstante, en caso de
                        que un menor desee solicitarnos información, sólo podrá hacerlo por sí mismo si es mayor de 14 años. En caso de los
                        menores de 14 años, deberá hacerlo quien ostente su patria potestad, tutor o representante legal, autorizando éste en
                        nombre de aquél el tratamiento de sus datos personales por parte de la empresa, de tal forma que quienes tengan a su
                        cargo menores de edad asumen la exclusiva responsabilidad de determinar los servicios y contenidos de este sitio web que
                        sean apropiados para la edad de los menores a su cargo.
                     </p>
                     
                     <p>
                        Átomo Virtual no asume ninguna responsabilidad en caso de que menores de edad nos faciliten sus datos incumpliendo las
                        exigencias de este párrafo.
                     </p>
                  </div>
                  
                  <div class="blog_details">
                     <h2>Enlaces desde otros sitios web a <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a></h2>
                     
                     <p>
                        Las personas o entidades que pretendan realizar o realicen un “hiperenlace” desde una página web ajena a Átomo Virtual a
                        cualquiera de las páginas de <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> deberán someterse a las siguientes condiciones:
                     </p>
                     
                     <p>No se permite la reproducción ni total ni parcial de ninguno de los servicios ni contenidos de <a  target="_blank" href="https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a>.
                     </p>
                     
                     <p>
                        No se incluirá ninguna manifestación falsa, inexacta o incorrecta sobre las páginas de <a  target="_blank" href="htttps://https://www.atomotecnovirtual.com">https://www.atomotecnovirtual.com</a> ni sobre los
                        servicios o contenidos del mismo.
                     </p>
                     
                     <p>
                        El establecimiento del “hiperenlace” no implicará la existencia de relaciones entre Átomo Virtual y el titular de la página web
                        o del portal desde el cual se realice, ni el conocimiento y aceptación de Átomo Virtual de los servicios y contenidos ofrecidos
                        en dicho portal.
                     </p>
                     
                     <p>
                        Átomo Virtual no será responsable de los contenidos o servicios puestos a disposición del público en la página web o portal
                        desde el cual se realice el “hiperenlace” ni de las informaciones y manifestaciones incluidas en las mismas.
                     </p>
                  </div>
                  
                  <div class="blog_details">
                     <h2>Cookies</h2>
                     
                     <p>
                        Las cookies son ficheros de texto de pequeño tamaño que se almacenan en el disco duro o en la memoria del ordenador que
                        accede o visita las páginas de determinados sitios web, de forma que se puedan conocer las preferencias del usuario al
                        volver a conectarse. Las cookies almacenadas en el disco duro del usuario no pueden leer los datos contenidos en él,
                        acceder a información personal ni leer las cookies creadas por otros proveedores.
                     </p>
                     
                     <p>Ver políticas de cookies en apartado “Política de cookies”.</p>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================ Blog Area end =================-->
   @include('layouts.cta')   
</main>

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
