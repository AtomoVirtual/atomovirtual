<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<main>
   
<!-- Slider Area Start-->
       <div class="services-area">
         <div class="container">

             <!-- Section-tittle -->
             <div class="row d-flex justify-content-center">
                 <div class="col-lg-8">
                     <div class="section-tittle text-center mb-80">
                         <span>Aviso Legal</span>
                         <h2>Política de Privacidad​</h2>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- Slider Area End-->
     
   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 posts-list">
               <div class="single-post">
                  <div class="blog_details">
                     <h2>Tratamiento de datos de carácter personal</h2>
                     
                     <p>
                        En Átomo Virtual nos preocupamos por los datos de carácter personal que tratamos y por el exacto cumplimiento de la
                        normativa vigente en materia de protección de datos de carácter personal (entre otras, Ley Orgánica 15/1999, de 13 de
                        diciembre, de Protección de Datos de Carácter Personal y Real Decreto 1720/2007, de 21 de diciembre, por el que se
                        aprueba el Reglamento de desarrollo de la LOPD).
                     </p>
                     
                     <p>
                        De acuerdo con ello, queremos informar al usuario de nuestro sitio web a través de esta política de privacidad sobre el
                        tratamiento de datos que llevamos a cabo en Átomo Virtual, de forma que cuente con la información necesaria para decidir si
                        quiere facilitarnos datos de carácter personal al cumplimentar un formulario o enviarnos un correo electrónico.
                     </p>
                  </div>  
                     
                  <div class="blog_details">
                        
                        <h2>Principio de calidad</h2>
                        
                        <p>
                           Se informa a los usuarios del sitio web <a href="www.atomovirtual.com">www.atomovirtual.com</a> de que los datos personales que, en su caso, faciliten a través del
                           formulario existente o de las direcciones de correo electrónico habilitadas para establecer contacto con nosotros, son
                           los adecuados, pertinentes y no excesivos en relación con el ámbito y las finalidades determinadas, explícitas y
                           legítimas para las que se obtienen.
                        </p>
                        
                       <p>
                           Estos datos personales no los utilizaremos para finalidades incompatibles con aquellas para las que los datos hubieran
                           sido recogidos. Los datos de carácter personal que recabamos serán exactos y puestos al día de forma que respondan con veracidad a la
                           situación actual del afectado.
                       </p>
                        
                        <p>
                           Procederemos de oficio a la cancelación de los datos de carácter personal que recabamos a través de nuestro sitio web
                           cuando hayan dejado de ser necesarios o pertinentes para la finalidad para la cual hubieran sido recabados o
                           registrados.
                        </p>
                        
                        <p>
                           Se entiende por cancelación el procedimiento en virtud del cual cesaremos en el uso de los datos. La cancelación
                           implicará el bloqueo de los datos, lo cual consiste en la identificación y reserva de los mismos con el fin de impedir
                           su tratamiento excepto para ponerlos a disposición de las Administraciones públicas, Jueces y Tribunales, para la
                           atención de las posibles responsabilidades nacidas del tratamiento y sólo durante el plazo de prescripción de dichas
                           responsabilidades. Transcurrido ese plazo procederemos a la supresión de los datos.
                        </p>
                  </div>
                     
                 <div class="blog_details">
                        <h2>Deber de información y consentimiento</h2>
                        
                        <p>
                           Se informa a los usuarios del sitio web <a href="www.atomovirtual.com">www.atomovirtual.com</a> de que los datos de carácter personal que se recaban a través del
                           formulario existente o a mediante la dirección de correo electrónico habilitada para establecer contacto con nosotros se
                           incorpora al fichero denominado “Contacto comercial”, previamente notificado a la Agencia Española de Protección de
                           Datos, del que Átomo Virtual es responsable, siendo utilizados para gestionar y responder las consultas, sugerencias,
                           solicitudes de información o los servicios que demanden los usuarios a través de nuestro sitio web.
                        </p>
                        
                        <p>
                           Todos los datos personales que solicitamos, aunque no estén marcados con asterisco, son obligatorios, de tal forma que
                           en caso de que el usuario no cumplimente todos los campos no podremos atender su solicitud.
                        </p>
                        
                        <p>
                           En el formulario de contacto se ha incorporado el correspondiente aviso legal o cláusula informativa en la que se indica
                           la finalidad o finalidades específicas a que se destinarán los datos recabados y demás aspectos exigidos por la
                           normativa vigente en materia de protección de datos de carácter personal. El usuario deberá leer y aceptar
                           obligatoriamente esa cláusula informativa antes de enviar su solicitud toda vez que en caso contrario no se hará
                           efectivo el envío.
                        </p>
                 </div>
                     
                  <div class="blog_details">
                        <h2>Seguridad</h2>
                        
                        <p>
                           De acuerdo a lo dispuesto en el artículo 9 LOPD y el Título VIII RLOPD, Átomo Virtual ha adoptado las medidas de seguridad
                           de índole técnica y organizativa necesarias que garantizan la seguridad de los datos de carácter personal y evitan su
                           alteración, pérdida, tratamiento o acceso no autorizado, habida cuenta del estado de la tecnología, la naturaleza de los
                           datos almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural.
                        </p>
                  </div>
                     
                     <div class="blog_details">
                        <h2>Deber de secreto</h2>
                        
                        <p>
                           De acuerdo con lo dispuesto en el artículo 10 de la LOPD todas aquellas personas que traten o tengan acceso a datos
                           personales de los usuarios guardarán secreto profesional respecto a los mismos y tienen el deber de guardarlos, siendo
                           obligaciones que tendrán una duración indefinida.
                        </p>
                     </div>
                     
                     <div class="blog_details">
                        <h2>Cesión o comunicación de datos</h2>
                        
                        <p>
                           En ningún caso los datos personales que nos proporcionan los usuarios de nuestro sitio web se comunicarán a terceros. Si
                           en algún momento fuera necesario realizar tal cesión de datos, de solicitaría previamente el consentimiento de los
                           usuarios salvo que el mismo no fuera necesario por así disponerlo una Ley o no ser necesario de acuerdo con las
                           excepciones previstas en el artículo 11.2 de la propia LOPD.
                        </p>
                     </div>
                     
                     <div class="blog_details">
                        <h2>Derechos de acceso, rectificación, cancelación y oposición</h2>
                        
                        Los usuarios del sitio web <a href="www.atomovirtual.com">www.atomovirtual.com</a> pueden ejercitar los derechos de acceso, rectificación, cancelación y oposición
                        dirigiendo una solicitud acompañada de fotocopia de su DNI a la siguiente dirección:
                        
                        Átomo Virtual, Calle Las escuelas, 10 oficina 4 C.P. 01001 de Vitoria (Álava) – España.
                        
                        Átomo Virtual responderá todas las solicitudes en los plazos y condiciones que exige la normativa vigente en materia de
                        protección de datos de carácter personal.
                     </div>
                                             
                     <div class="blog_details">
                        <h2>Finalidad</h2>                
                        
                        <p>
                           En Átomo Virtual. tratamos la información que nos facilitan las personas interesadas con el fin de enviar información
                           relevante sobre formaciones, cursos y/o servicios ofrecidos por Átomo Virtual, adicionalmente se podrá enviar publicidad
                           del mismo mediante correo electrónico, previa autorización del interesado. No se tomarán decisiones automatizadas en
                           base a dicho perfil. Átomo Virtual no prevé la transferencia de estos datos a terceros. Los datos personales proporcionados
                           se conservarán mientras no se solicite su supresión por el interesado y, en este último caso, siempre y cuando no lo
                           prohíba expresamente la normativa vigente en materia de protección de datos. Átomo Virtual no se hace responsable de
                           aquellos datos facilitados voluntaria y adicionalmente y que no sean requeridos, no siendo tenidos en cuenta para la
                           finalidad descrita.
                        </p>
                     </div>
                     
                     <div class="blog_details">
                        <h2>Legitimación</h2>
                        
                        <p>
                           La legitimación del tratamiento de sus datos personales encuentra su fundamento en el consentimiento del interesado. Los
                           datos personales solicitados son de carácter obligatorio, por lo que su no cumplimentación supone la imposibilidad de su
                           inclusión en los ficheros anteriormente descritos y el cumplimiento de las finalidades definidas en el párrafo anterior.
                        </p>
                     </div>
                     
                     <div class="blog_details">
                        <h2>Destinatarios</h2>
                        
                        <p>El destinatario de sus datos personales recogidos mediante la siguiente ficha será Átomo Virtual</p>
                     </div>
                     
                     <div class="blog_details">
                        <h2>Derechos</h2>
                        
                        <p>
                           Cualquier persona tiene derecho a obtener confirmación sobre si, en Átomo Virtual, estamos tratando datos personales que
                           les conciernan, o no.
                        </p>
                        
                        <p>
                           Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los
                           datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para
                           el cumplimiento de los fines para los que fueron recogidos y en cumplimiento de la legislación vigente.
                        </p>
                        
                        <p>
                           En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo
                           caso, únicamente, se conservarán para el ejercicio o la defensa de reclamaciones.
                        </p>
                        
                        <p>
                           En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse
                           al tratamiento de sus datos.
                        </p>
                        
                        <p>
                           Átomo Virtualdejará de tratar los datos, salvo por motivos legítimos imperiosos o en el ejercicio o la defensa de posibles
                           reclamaciones.
                        </p>
                        
                        <p>
                           Asimismo, el interesado tiene derecho a recibir los datos personales que ha facilitado a Átomo Virtual en un formato
                           estructurado, de uso común y legible por máquina. Este último derecho quedará limitado por las siguientes condiciones:
                           que los datos sobre los que recae este derecho, hayan sido facilitados por la persona interesada; que sus datos sean
                           tratados por Átomo Virtual de manera automatizada (medios informáticos).
                        </p>
                     </div>
                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================ Blog Area end =================-->
   @include('layouts.cta')   

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
