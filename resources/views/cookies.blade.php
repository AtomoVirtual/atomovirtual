<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<main>
   
<!-- Slider Area Start-->
       <div class="services-area">
         <div class="container">

             <!-- Section-tittle -->
             <div class="row d-flex justify-content-center">
                 <div class="col-lg-8">
                     <div class="section-tittle text-center mb-80">
                         <span>Aviso Legal</span>
                         <h2>Política de Cookies​</h2>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- Slider Area End-->
     
   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 posts-list">
               <div class="single-post">
                  <div class="blog_details">
                     <div class="blog_details">
                        <h2>¿Qué son las cookies?</h2>
                        
                        <p>
                           Las cookies son ficheros de texto de pequeño tamaño que se almacenan en el disco duro o en la memoria del ordenador que
                           accede o visita las páginas de determinados sitios web, de forma que se puedan conocer las preferencias del usuario al
                           volver a conectarse. Las cookies almacenadas en el disco duro del usuario no pueden leer los datos contenidos en él,
                           acceder a información personal ni leer las cookies creadas por otros proveedores.
                        </p>
                     </div>
                     
                     <div class="blog_details">
                        <h2>¿Qué tipo de cookies existen?</h2>
                        
                        <strong>Según la entidad que las gestione:</strong>
                        
                        <p>Cookies propias: son las generadas por la página que está visitando.</p>
                        
                        <p>Cookies de terceros: son las generadas por servicios o proveedores externos como Facebook, Twitter, Google, etc.</p>
                        
                        <strong>Según el plazo de tiempo que permanecen activas:</strong>
                        
                        <p>
                           Cookies de sesión: son un tipo de cookies diseñadas para recabar y almacenar datos mientras el usuario accede a una
                           página web.
                        </p>
                        
                        <p>
                           Cookies persistentes: son un tipo de cookies en el que los datos siguen almacenados en el terminal y pueden ser
                           accedidos y tratados durante un periodo definido por el responsable de la cookie.
                        </p>
                        
                        <strong>Según su finalidad:</strong>
                        
                        <p>
                           Cookies técnicas: son las más elementales y permiten, entre otras cosas, saber cuándo está navegando un humano o una
                           aplicación automatizada, cuándo navega un usuario anónimo y uno registrado, tareas básicas para el funcionamiento de
                           cualquier web dinámica.
                        </p>
                        
                        <p>
                           Cookies de personalización: son aquéllas que permiten al usuario acceder al servicio con algunas características de
                           carácter general predefinidas en su terminal o que el propio usuario defina. Por ejemplo, el idioma, el tipo de
                           navegador a través del cual accede al servicio, el diseño de contenidos seleccionado, geolocalización del terminal y la configuración regional desde donde se accede al servicio, etc.
                        </p>
                        
                        
                        <p>
                           Cookies de análisis: recogen información sobre el tipo de navegación que está realizando, las secciones que más utiliza,
                           productos consultados, franja horaria de uso, idioma, etc.
                        </p>
                        
                        <p>Cookies publicitarias: muestran publicidad en función de su navegación, su país de procedencia, idioma, etc.</p>
                        
                        <p>
                           Cookies de publicidad comportamental: son aquellas que permiten la gestión eficaz de los espacios publicitarios que se
                           han incluido en la página web o aplicación desde la que se presta el servicio. Estas cookies almacenan información del
                           comportamiento de los usuarios a través de la observación permitiendo posteriormente desarrollar un perfil específico
                           para mostrar publicidad en función del mismo.
                        </p>
                     </div>
                     
                     
                     <div class="blog_details">
                        <h2>¿Cómo deshabilitar las cookies?</h2>
                        
                        <p>
                           El modo en que el usuario puede deshabilitar cookies dependerá del navegador o navegadores que utilice. Todos los
                           navegadores permiten cambiar la configuración relacionada con las cookies. A continuación indicamos los pasos a dar para
                           llegar a dicha configuración en los principales navegadores utilizados en la actualidad, si bien los pasos que se
                           describen pueden variar en función de la versión del navegador:
                        </p>
                        
                        <p><strong>– Firefox:</strong> http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we</p>
                        
                        <p>
                           Para acceder a la configuración de cookies del navegador Firefox siga estos pasos (pueden variar en función de la
                           versión del navegador):
                        </p>
                       

                           <ol class="ordered-list">
                              <li><span>Vaya a Opcioneso Preferencias según su sistema operativo.</span></li>
                              <li><span>Haga click en Privacidad.</span></li>
                              <li><span>En Historialelija Usar una configuración personalizada para el historial.</span></li>
                              <li><span>Ahora verá la opción Aceptar cookies, puede activarla o desactivarla según sus preferencias.</span></li>
                           </ol>
                        
                        
                        
                        
                        
                        <p><strong>– Internet Explorer:</strong> http://windows.microsoft.com/es-es/windows-vista/Block-or-allow-cookies</p>
                        
                        <p>
                           Para acceder a la configuración de cookies del navegador Internet Explorer siga estos pasos (pueden variar en función de
                           la versión del navegador):
                        </p>
                        
                        <ol class="ordered-list">
                           <li><span>Vaya a Herramientas, Opciones de Internet.</span></li>
                           <li><span>Haga click en Privacidad.</span></li>
                           <li><span>Mueva el deslizador hasta ajustar el nivel de privacidad que desee.</span></li>
                        </ol>

                        
                        
                        
                        <p><strong>– Chrome:</strong> https://support.google.com/chrome/answer/95647?hl=es</p>
                        
                        <p>
                           Para acceder a la configuración de cookies del navegador Chrome siga estos pasos (pueden variar en función de la versión
                           del navegador):
                        </p>

                        <ol class="ordered-list">
                           <li><span>Vaya a Configuración o Preferencias mediante el menú Archivo o bien pinchando el icono de personalización que aparece
                           arriba a la derecha.</span></li>
                           <li><span>Verá diferentes secciones, pinche la opción Mostrar opciones avanzadas.</span></li>
                           <li><span>Vaya a Privacidad, Configuración de contenido.</span></li>
                           <li><span>Seleccione Todas las cookiesy los datos de sitios.</span></li>
                           <li><span>Aparecerá un listado con todas las cookiesordenadas por dominio. Para que le sea más fácil encontrar las cookiesde un
                           determinado dominio introduzca parcial o totalmente la dirección en el campo Buscar cookies.</span></li>
                           <li><span>Tras realizar este filtro aparecerán en pantalla una o varias líneas con las cookiesde la web solicitada. Ahora sólo
                           tiene que seleccionarla y pulsar la Xpara proceder a su eliminación.</span></li>
                        </ol>
                        
                        
                     </div>
                     
                     <div class="blog_details">
                        <h2>¿Qué consecuencias tiene deshabilitar las cookies?</h2>
                        
                        <p>
                           En caso de que el usuario impida la activación de las cookies o desactive en caso de haberlas autorizado previamente,
                           podrá afectar al funcionamiento de algunas funcionalidades del sitio web como pueden ser, entre otras, que no se guarde
                           información sobre la aceptación del aviso inicial de cookies por lo que se le volverá a mostrar en la próxima visita.
                           Este es el caso de las cookies mencionadas en el apartado de “funcionamiento y personalización”.
                        </p>
                        
                        <p>
                           Por otro lado, en caso de que el usuario impida la activación de las cookies o desactive en caso de haberlas autorizado
                           previamente, hará que no podamos obtener información sobre navegación de los usuarios a través del mismo, lo cual
                           impedirá o dificultará la mejora de la información y contenidos que publicamos así como su presentación. Este es el caso
                           de las cookies mencionadas en el apartado de “Google Analytics”.
                        </p>
                     </div>
                     
                     
                     <div class="blog_details">
                        <h2>Cambios o actualizaciones en nuestras políticas sobre cookies</h2>
                        
                        <p>
                           Es posible que la estructura y funcionalidades del presente sitio web se modifique para añadir nuevos apartados,
                           servicios o contenidos, lo cual puede suponer que se modifique el uso y, por tanto, nuestra sobre cookies. Por eso
                           recomendamos a los usuarios que cada vez que accedan a nuestro sitio web revisen las políticas de cookies publicadas por
                           si se hubiera producido algún cambio desde su última visita.
                        </p>
                     </div>
                     
                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================ Blog Area end =================-->
   @include('layouts.cta')   
</main>

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
