<!-- Google Tag Manager (noscript) -->
<noscript>
<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQQFWG2"
    height="0" width="0" style="display:none;visibility:hidden">
</iframe>
</noscript>
    <!-- End Google Tag Manager (noscript) -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="{{URL::asset('assets_web/img/logo/logo.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
