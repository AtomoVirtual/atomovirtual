@include('layouts.modals')
<footer>
    <!-- Footer Start-->
    <div class="footer-main" data-background="{{URL::asset('assets_web/img/shape/footer_bg.png')}}">
        <div class="footer-area footer-padding">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="col-lg-3 col-md-4 col-sm-8">
                        <div class="single-footer-caption mb-50">
                            <div class="single-footer-caption mb-30">
                                <!-- logo -->
                                <div class="footer-logo">
                                    <div class="logo">
                                        <a href="{{route('home.index')}}">
                                            <p><span class="flaticon-central"></span>Átomo Virtual</p>
                                        </a>
                                    </div>
                                </div>
                                <div class="footer-tittle mb-30">
                                    <ul>
                                        <li><a>Barcelona, España</a></li>
                                        <li><a href="mailto:contacto@atomotecnovirtual.com">contacto@atomotecnovirtual.com</a></li>
                                        <li><a href="https://api.whatsapp.com/send?phone=34610695232">+34 6106 95232</a></li>
                                    </ul>
                                </div>
                                <div class="footer-social">
                                    <a target="_blank" href="https://www.facebook.com/atomvirtual/"><i class="fab fa-facebook-f"></i></a>
                                    <a target="_blank" href="https://www.linkedin.com/in/atomotecnovirtual/"><i class="fab fa-twitter"></i></a>
                                    <a target="_blank" href="https://www.instagram.com/atomovirtual/"><i class="fab fa-instagram"></i></a>
                                    <a target="_blank" href="https://www.linkedin.com/company/atomotecnovirtual/"><i class="fab fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Enlaces</h4>
                                <ul>
                                    <li><a href="{{route('home.about')}}">Nosotros</a></li>
                                    <li><a href="{{route('home.services')}}">Servicios</a></li>
                                    <li><a href="{{route('home.contact')}}">Contacto</a></li>
                                    <!--
                                    <li><a href="{{route('home.blog')}}">Blog</a></li>
                                    -->
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-7">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Soporte</h4>
                                <ul>
                                    <li><a href="{{route('home.terms')}}">Términos y Condiciones</a></li>
                                    <li><a href="{{route('home.policy')}}">Política de Privacidad</a></li>
                                    <li><a href="{{route('home.cookies')}}">Política de Cookies</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Actualizaciones</h4>
                                <ul>
                                    <li><a>Recibe notificaciones de nuestro contenido.</a></li>
                                </ul>
                                <form class="form-contact" action="{{route('home.contact.store')}}" method="POST"  >
                                {{ csrf_field() }}
                                    <div class="mt-1">
                                        <input type="email" name="email" placeholder="Deja tu email" onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Email'" required class="single-input">
                                    </div>
                                    <div class="hero__btn mt-3" data-animation="fadeInLeft" data-delay=".8s">
                                        <button type="submit" class="button btn hero-btn">Notificarme</button>
                                    </div>
                                    <input type="hidden" name="hidden_email" value="1">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-bottom aera -->
        <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-12 ">
                            <div class="footer-copy-right text-center">
                                <p>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;
                                    <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados |
                                    Hecho con <i class="ti-heart" aria-hidden="true"></i> por <a
                                        href="{{route('home.index')}}" target="_blank">Átomo Virtual</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->

</footer>
