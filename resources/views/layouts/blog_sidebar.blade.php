<div class="blog_right_sidebar">

    <aside class="single_sidebar_widget post_category_widget">
       <h4 class="widget_title">Categorías</h4>
       <ul class="list cat-list">
        @foreach($categories as $category)
            <li>
                <a href="{{route('home.blog_cat', $category->id )}}" class="d-flex">
                    <p>{{$category->title}}</p>
                </a>
            </li>
        @endforeach
        </ul>
    </aside>

    <aside class="single_sidebar_widget popular_post_widget">
       <h3 class="widget_title">Posts Aleatorios</h3>
        @foreach($posts_randoms as $pr)
        <div class="media post_item">
            <img src="{{URL::asset('assets_web/img/post/post_1.png')}}" alt="post">
            <div class="media-body">
                <a href="{{route('home.post', $pr->id )}}">
                    <h3>{{$pr->title}}</h3>
                </a>
                <p>{{$pr->created_at}}</p>
            </div>
        </div>
       @endforeach
    </aside>
<!--
    <aside class="single_sidebar_widget tag_cloud_widget">
       <h4 class="widget_title">Etiquetas</h4>
       <ul class="list">
          <li>
             <a href="#">desarrollo</a>
          </li>
          <li>
             <a href="#">diseño</a>
          </li>
          <li>
             <a href="#">bases de datos</a>
          </li>
          <li>
             <a href="#">funciones</a>
          </li>
          <li>
             <a href="#">estructuras</a>
          </li>
          <li>
             <a href="#">testing</a>
          </li>
       </ul>
    </aside>
    <aside class="single_sidebar_widget instagram_feeds">
       <h4 class="widget_title">Instagram</h4>
       <ul class="instagram_row flex-wrap">
          <li>
             <a href="#">
                <img class="img-fluid" src="{{URL::asset('assets_web/img/post/post_5.png')}}" alt="">
             </a>
          </li>
          <li>
             <a href="#">
                <img class="img-fluid" src="{{URL::asset('assets_web/img/post/post_6.png')}}" alt="">
             </a>
          </li>
          <li>
             <a href="#">
                <img class="img-fluid" src="{{URL::asset('assets_web/img/post/post_7.png')}}" alt="">
             </a>
          </li>
          <li>
             <a href="#">
                <img class="img-fluid" src="{{URL::asset('assets_web/img/post/post_8.png')}}" alt="">
             </a>
          </li>
          <li>
             <a href="#">
                <img class="img-fluid" src="{{URL::asset('assets_web/img/post/post_9.png')}}" alt="">
             </a>
          </li>
          <li>
             <a href="#">
                <img class="img-fluid" src="{{URL::asset('assets_web/img/post/post_10.png')}}" alt="">
             </a>
          </li>
       </ul>
    </aside>
-->

    <aside class="single_sidebar_widget newsletter_widget">
       <h4 class="widget_title">Newsletter</h4>

       <form action="{{route('home.contact.store')}}" method="POST"  >
          {{ csrf_field() }}
          <div class="form-group">
             <input type="email" name="email" class="form-control" onfocus="this.placeholder = ''"
                onblur="this.placeholder = 'Deja tu Email'" placeholder='Deja tu Email' required>
          </div>
          <button class="button rounded-0 primary-bg text-white w-100 btn_1 boxed-btn"
             type="submit">Suscribirse</button>
             <input type="hidden" name="hidden_email" value="1">
      </form>
    </aside>
 </div>
