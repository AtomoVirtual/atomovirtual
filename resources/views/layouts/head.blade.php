<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Átomo Virtual</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Creamos experiencias web increibles, Te ayudamos a crear una presencia solida en internet y que consigas el éxito con tu proyecto.">
    <meta name="author" content="Átomo Virtual">
    <meta name="keywords" content="Desarrollo Web, Aplicaciones Moviles, Cursos Online, atomo virtual">

    <link rel="shortcut icon" type="image/x-icon" href="{{URL::asset('assets_web/img/favicon.ico')}}">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/slick.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets_web/css/style.css')}}">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KQQFWG2');
</script>
    <!-- End Google Tag Manager -->

</head>
