<header>
    <!-- Header Start -->
    <div class="header-area header-transparrent ">
        <div class="main-header header-sticky">
            <div class="container">
                <div class="row align-items-center">
                    <!-- Logo -->
                    <div class="col-xl-3 col-lg-3 col-md-8">
                        <div class="logo">
                            <a href="{{route('home.index')}}"><p><span class="flaticon-central"></span>Átomo Virtual</p></a>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-2">
                        <!-- Main-menu -->
                        <div class="main-menu f-right d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a href="{{route('home.index')}}">Inicio</a></li>
                                    <li><a href="{{route('home.about')}}">Nosotros</a></li>
                                    <li><a href="{{route('home.services')}}">Servicios</a></li>
                                    <li><a href="{{route('home.contact')}}">Contacto</a></li>
                                    <!--
                                    <li><a href="{{route('home.blog')}}">Blog</a></li>
                                    -->
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-3">
                        <div class="header-right-btn f-right d-none d-lg-block">
                            <a href="tel:+34610695232" class="btn header-btn">Llámanos</a>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>
