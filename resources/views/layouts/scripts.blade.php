    <!-- JS here -->

    <!-- All JS Custom Plugins Link Here here -->
    <script src="{{URL::asset('assets_web/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{URL::asset('assets_web/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/popper.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/bootstrap.min.js')}}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{URL::asset('assets_web/js/jquery.slicknav.min.js')}}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{URL::asset('assets_web/js/owl.carousel.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/slick.min.js')}}"></script>
    <!-- Date Picker -->
    <script src="{{URL::asset('assets_web/js/gijgo.min.js')}}"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="{{URL::asset('assets_web/js/wow.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/animated.headline.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/jquery.magnific-popup.js')}}"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="{{URL::asset('assets_web/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/jquery.sticky.js')}}"></script>

    <!-- contact js -->
    <script src="{{URL::asset('assets_web/js/contact.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/jquery.form.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/jquery.validate.min.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/mail-script.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/jquery.ajaxchimp.min.js')}}"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="{{URL::asset('assets_web/js/plugins.js')}}"></script>
    <script src="{{URL::asset('assets_web/js/main.js')}}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5c90fdf9101df77a8be3630d/1eb9ilueu';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->


    @if(Session::get('success'))
    <script>
      $('#SuccessModal').modal('show');
    </script>
    @endif

    @if(Session::get('error'))
    <script>
      $('#ErrorModal').modal('show');
    </script>
    @endif

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171012127-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171012127-1');
</script>
