<div class="modal fade SuccessModal" tabindex="-1" role="dialog" aria-labelledby="SuccessModal" aria-hidden="true" id="SuccessModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <img src="{{URL::asset('assets_web/img/logo/check.png')}}" alt="" width="50px">
                <h2>Solicitud Exitosa</h2>
            </div>
            <div class="modal-footer">
            </div>
      </div>
    </div>
</div>
<div class="modal fade ErrorModal" tabindex="-1" role="dialog" aria-labelledby="ErrorModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
          <img src="{{URL::asset('assets_web/img/logo/error.png')}}" alt="" width="50px">
          <h2>Solicitud Errónea</h2>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
</div>
