<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<main>
    <!-- Slider Area Start-->
       <div class="services-area">
         <div class="container">

             <!-- Section-tittle -->
             <div class="row d-flex justify-content-center">
                 <div class="col-lg-8">
                     <div class="section-tittle text-center mb-80">
                         <span>Post</span>
                         <h2>{{$post->title}}​</h2>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <!-- Slider Area End-->

    <!--================Blog Area =================-->
   <section class="blog_area single-post-area section-padding">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 posts-list">
               <div class="single-post">
                  <div class="feature-img">
                     <img class="img-fluid" src="{{URL::asset($post->img_url)}}" alt="">
                  </div>
                  <div class="blog_details">
                     <h2>{{$post->title}}
                     </h2>
                     <ul class="blog-info-link mt-3 mb-4">
                         <li><a href="#"><i class="fa fa-user"></i> {{$post->cat_title}}</a></li>
                     </ul>
                     <p class="excert">
                        {{$post->content}}
                     </p>

                  </div>
               </div>
               <div class="navigation-top">

                  <div class="navigation-area">
                     <div class="row">
                        <div
                           class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                           @if(($post->id - 1)==0)
                           @else
                           <div class="thumb">
                                <a href="{{route('home.post', $post->id - 1)}}">
                                <img class="img-fluid" src="{{URL::asset($post->img_url)}}" alt="">
                                </a>
                            </div>
                           <div class="arrow">
                                <a href="{{route('home.post', $post->id - 1)}}">
                                <span class="lnr text-white ti-arrow-left"></span>
                                </a>
                            </div>
                           <div class="detials">
                            <p>Post Anterior</p>
                            <a href="{{route('home.post', $post->id - 1)}}">
                               <h4>{{$post->title}}</h4>
                            </a>
                         </div>
                           @endif
                        </div>

                        <div
                           class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                            @if(($post->id + 1) > sizeof($posts))
                            @else
                            <div class="detials">
                                <p>Post Siguiente</p>
                                <a href="{{route('home.post', $post->id + 1)}}">
                                   <h4>{{$post->title}}</h4>
                                </a>
                             </div>
                             <div class="arrow">
                                <a href="{{route('home.post', $post->id + 1)}}">
                                   <span class="lnr text-white ti-arrow-right"></span>
                                </a>
                             </div>
                             <div class="thumb">
                                <a href="{{route('home.post', $post->id + 1)}}">
                                   <img class="img-fluid" src="{{URL::asset($post->img_url)}}" alt="">
                                </a>
                            </div>
                            @endif

                        </div>
                     </div>
                  </div>
               </div>
               <!--
               <div class="blog-author">
                  <div class="media align-items-center">
                     <img src="{{URL::asset('assets_web/img/blog/author.png')}}" alt="">
                     <div class="media-body">
                        <a href="#">
                           <h4>Nombre del Autor</h4>
                        </a>
                        <p>Second divided from form fish beast made. Every of seas all gathered use saying you're, he
                           our dominion twon Second divided from</p>
                     </div>
                  </div>
               </div>

               <div class="comments-area">
                  <h4>03 Comentarios</h4>
                  <div class="comment-list">
                     <div class="single-comment justify-content-between d-flex">
                        <div class="user justify-content-between d-flex">
                           <div class="thumb">
                              <img src="{{URL::asset('assets_web/img/comment/comment_1.png')}}" alt="">
                           </div>
                           <div class="desc">
                              <p class="comment">
                                 Multiply sea night grass fourth day sea lesser rule open subdue female fill which them
                                 Blessed, give fill lesser bearing multiply sea night grass fourth day sea lesser
                              </p>
                              <div class="d-flex justify-content-between">
                                 <div class="d-flex align-items-center">
                                    <h5>
                                       <a href="#">Emilly Blunt</a>
                                    </h5>
                                    <p class="date">December 4, 2017 at 3:12 pm </p>
                                 </div>
                                 <div class="reply-btn">
                                    <a href="#" class="btn-reply text-uppercase">responder</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="comment-list">
                     <div class="single-comment justify-content-between d-flex">
                        <div class="user justify-content-between d-flex">
                           <div class="thumb">
                              <img src="{{URL::asset('assets_web/img/comment/comment_2.png')}}" alt="">
                           </div>
                           <div class="desc">
                              <p class="comment">
                                 Multiply sea night grass fourth day sea lesser rule open subdue female fill which them
                                 Blessed, give fill lesser bearing multiply sea night grass fourth day sea lesser
                              </p>
                              <div class="d-flex justify-content-between">
                                 <div class="d-flex align-items-center">
                                    <h5>
                                       <a href="#">Emilly Blunt</a>
                                    </h5>
                                    <p class="date">December 4, 2017 at 3:12 pm </p>
                                 </div>
                                 <div class="reply-btn">
                                    <a href="#" class="btn-reply text-uppercase">responder</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="comment-list">
                     <div class="single-comment justify-content-between d-flex">
                        <div class="user justify-content-between d-flex">
                           <div class="thumb">
                              <img src="{{URL::asset('assets_web/img/comment/comment_3.png')}}" alt="">
                           </div>
                           <div class="desc">
                              <p class="comment">
                                 Multiply sea night grass fourth day sea lesser rule open subdue female fill which them
                                 Blessed, give fill lesser bearing multiply sea night grass fourth day sea lesser
                              </p>
                              <div class="d-flex justify-content-between">
                                 <div class="d-flex align-items-center">
                                    <h5>
                                       <a href="#">Emilly Blunt</a>
                                    </h5>
                                    <p class="date">December 4, 2017 at 3:12 pm </p>
                                 </div>
                                 <div class="reply-btn">
                                    <a href="#" class="btn-reply text-uppercase">responder</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="comment-form">
                  <h4>Comentar este Post</h4>
                  <form class="form-contact comment_form" action="#" id="commentForm">
                     <div class="row">
                        <div class="col-12">
                           <div class="form-group">
                              <textarea class="form-control w-100" name="comment" id="comment" cols="30" rows="9"
                                 placeholder="Write Comment"></textarea>
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <input class="form-control" name="name" id="name" type="text" placeholder="Name">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <input class="form-control" name="email" id="email" type="email" placeholder="Email">
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="form-group">
                              <input class="form-control" name="website" id="website" type="text" placeholder="Website">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <button type="submit" class="button button-contactForm btn_1 boxed-btn">Enviar Comentario</button>
                     </div>
                  </form>
               </div>
-->
            </div>
            <div class="col-lg-4">
                @include('layouts.blog_sidebar')
            </div>
         </div>
      </div>
   </section>
   <!--================ Blog Area end =================-->



   @include('layouts.cta')
</main>

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
