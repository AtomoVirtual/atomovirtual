<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

    <main>
        <!-- Slider Area Start-->
        <div class="services-area">
            <div class="container">

                <!-- Section-tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center mb-80">
                            <span>Servicios</span>
                            <h2>Todo lo que podemos hacer por ti​</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Area End-->
        <!-- What We do start-->
        <div class="what-we-do">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-10">
                            <div class="do-icon">
                                <span class="flaticon-null"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Desarrollo Móvil</h4>
                                <p>Creamos aplicaciones móviles diseñadas a la medida, cuéntanos que necesitas y diseñaremos el mejor
                                    producto para superar tus expectativas.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero una App</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-null-1"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Desarrollo Web</h4>
                                <p>Innovamos con los mejores diseños para impactar a tus clientes, cada proyecto es único, personalizado
                                    y optimizado para posicionar en los buscadores.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero una Web</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-null-3"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Cursos Online</h4>
                                <p>Nos gusta compartir nuestros conocimientos y ver como los demás tienen éxito, si quieres aprender a
                                    crear aplicaciones móviles y sitios web contáctanos.</p>
                            </div>
                            <div class="do-btn">
                                <a href="{{route('home.contact')}}"><i class="ti-arrow-right"></i> Quiero Aprender</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- What We do End-->

        <!-- Generating Start -->
        <div class="generating-area visite-padding2">
            <div class="container">
                 <!-- Section-tittle -->
                 <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <h2>Servicios Detallados​​</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-null-2"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Posición en la Web</h4>
                                <p>Haremos que tu marca o producto sea potente en los buscadores y así atraer muchos mas clientes. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-pantalla"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Vende Mucho Más</h4>
                                <p>Webs atractivas con productos que impresionen al usuario para convertirlos en clientes directos.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-codigo"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Autoadministrables</h4>
                                <p>Todos nuestros proyectos son autoadministrables, y que puedas modificar cada aspecto del mismo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-null-4"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Creación de Canales</h4>
                                <p>Creamos tus perfiles en las redes sociales que necesites y los optimizamos para que sean impactantes. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-crecimiento"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Analítica Web</h4>
                                <p>Conectamos tu web con la herramienta de Google Analytics, así podrás ver el impacto del proyecto. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="single-generating text-center mb-30">
                            <div class="generating-icon">
                                <span class="flaticon-soporte-tecnico"></span>
                            </div>
                            <div class="generating-cap">
                                <h4>Servicio de Soporte</h4>
                                <p>Disponemos de servicio de soporte y mantenimiento para todos nuestros proyectos a la medida. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Generating End -->
        @include('layouts.cta')   

    </main>

@include('layouts.footer')
@include('layouts.scripts')

</body>
</html>
