@extends('admin.layouts.app')

@section('htmlheader_title')
	Usuarios
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="card mb-4">
    </div>
    <div class="card mb-8">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>Contactos
            </div>
        </div>
        <div class="card-body">
            <table  class="table table-bordered " >
                <thead>
                    <tr >
                        <th>Nombre</th>
                        <th>Mail</th>
                        <th>Creado</th>
                        <th>Actualizado</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr >
                        <th>Nombre</th>
                        <th>Mail</th>
                        <th>Creado</th>
                        <th>Actualizado</th>
                        <th>Acción</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        <td><center>
                        <form action="{{route('admin.users.destroy', $user->id)}}" method="get" style="display:inline">
                            <input type="submit" value="Eliminar" class="btn btn-danger">
                        </form>
                        </center>
                        </td>
                    </tr>
                    @endforeach
                </tbody>


            </table>
        </div>
    </div>
</div>
@endsection
