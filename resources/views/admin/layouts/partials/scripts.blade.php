<!-- jQuery -->
<script src="{{ asset('assets_adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets_adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets_adminlte/js/adminlte.min.js') }}"></script>

<script src="{{ asset('assets_adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets_adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script>
      $(".alert").delay(6000).slideUp(200, function() {
        $(this).alert('close');
      });


  $(function () {
    $('#example1').DataTable({
          "paging": true,
          "order": [],
          "language": {
            "pageLength": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info":           "Mostrando _START_ de _END_ de _TOTAL_ resultados",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    })

    $('#example2').DataTable({
          "paging": true,
          "order": [],
          "language": {
            "pageLength": "Ver _MENU_ registros por página",
            "zeroRecords": "No se encontraron resultados",
            "info":           "Mostrando _START_ de _END_ de _TOTAL_ resultados",
            "infoEmpty": "Sin Registros",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                  "first":      "Primero",
                  "last":       "Último",
                  "next":       "Siguiente",
                  "previous":   "Atras"
            },
            "search":         "Buscar:"
        }
    })
  })



$(document).ready(function(){
                /** add active class and stay opened when selected */
                var url = window.location;
                // for sidebar menu entirely but not cover treeview
                $('ul.sidebar-menu a').filter(function() {
                     return this.href == url;
                }).parent().addClass('active');

                // for treeview
                $('ul.treeview-menu a').filter(function() {
                     return this.href == url;
                }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
            });

</script>
