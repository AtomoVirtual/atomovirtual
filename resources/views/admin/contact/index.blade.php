@extends('admin.layouts.app')

@section('htmlheader_title')
	Roles
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop



@section('main-content')

<div class="container-fluid">
    <div class="card mb-4">
    </div>
    <div class="card mb-8">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>Contactos
            </div>
        </div>
        <div class="card-body">
            <table id="example2" class="table table-bordered " >
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Teléfono</th>
                        <th class="cell-hyphens">Mensaje</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Email</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Teléfono</th>
                        <th >Mensaje</th>
                        <th>Acción</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($contacts as $contact)
                    <tr>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->lastname }}</td>
                        <td>{{ $contact->phone }}</td>
                        <td class="cell-hyphens">{{ $contact->message }}</td>
                        <td><center>
                            <form action="{{route('admin.contact.destroy', $contact->id)}}" method="get" style="display:inline">
                                <input type="submit" value="Eliminar" class="btn btn-danger">
                            </form>
                        </center></td>
                    </tr>
                    @endforeach
                </tbody>


            </table>
        </div>
    </div>
</div>


@endsection
