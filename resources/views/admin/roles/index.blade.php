@extends('admin.layouts.app')

@section('htmlheader_title')
	Roles
@endsection
@section('contentheader_title') {{-- TITULO DEL CONTENIDO DE LA VISTA --}}

@stop


@section('main-content')

<div class="container-fluid">
    <div class="card mb-4">
    </div>
    <div class="card mb-8">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <i class="header-icon lnr-laptop-phone mr-3 text-muted opacity-6"> </i>Contactos
            </div>
        </div>
        <div class="card-body">
            <table  class="table table-bordered " >
                <thead>
                    <tr >
                        <th>Nombre</th>
                        <th>Guard Name</th>
                        <th>Creado</th>
                        <th>Actualizado</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr >
                        <th>Nombre</th>
                        <th>Guard Name</th>
                        <th>Creado</th>
                        <th>Actualizado</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($roles as $role)
                    <tr>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->guard_name }}</td>
                        <td>{{ $role->created_at }}</td>
                        <td>{{ $role->updated_at }}</td>
                    </tr>
                    @endforeach
                </tbody>


            </table>
        </div>
    </div>
</div>
@endsection
