<!doctype html>
<html class="no-js" lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

    <main>

        <!-- Slider Area Start-->
        <div class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center"
                    data-background="{{URL::asset('assets_web/img/hero/about_shape1.png')}}">
                    <div class="container">
                        <div class="row d-flex align-items-center">
                            <div class="col-12 text-center">
                                <div class="hero__caption">
                                    <h1 data-animation="fadeInLeft" data-delay=".4s">¿Qué es<br> Átomo Virtual?</h1>
                                    <div class="col-lg-8 offset-lg-2"><p data-animation="fadeInLeft" data-delay=".6s" class="text-center">Somos una agencia de desarrollo web, donde impulsamos tus ventas diseñando sitios webs y plataformas optimizadas para posicionar en internet, cuéntanos tu idea y hagámosla realidad.</p></div>
                                    <img src="{{URL::asset('assets_web/img/about/icono.svg')}}" class="mx-auto d-block icot" alt="...">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider Area End-->

        <!-- We Create Start -->
        <div class="we-create-area we-padding">
            <div class="container">
                <div class="row d-flex align-items-end">
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-img mb-30">
                            <img src="{{URL::asset('assets_web/img//about/celebrar.svg')}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-cap">
                            <h3>Logramos que nuestros clientes consigan el éxito.</h3>
                            <p>Desde 2016 desarrollamos webs especializadas para nuestros clientes. A día de hoy seguimos creciendo y cristalizando proyectos, seguimos actualizando las tecnologías para brindar la mejor oportunidad digital.</p>
                            <a href="{{route('home.contact')}}" class="btn">Contáctanos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- We Create End -->

        <!-- What We do start-->
        <div class="what-we-do we-padding">
            <div class="container">
                <!-- Section-tittle -->
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <h2>Los problemas de nuestros clientes</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-10">
                            <div class="do-icon">
                                <span class="flaticon-retroalimentacion"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Mala Imagen</h4>
                                <p>Poca esencia en su marca y sitio web que transmite poca profesionalidad e imagen obsoleta.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-bloquear-usuario"></span>
                            </div>
                            <div class="do-caption">
                                <h4>Falta de Ventas</h4>
                                <p>Web con poco tráfico y que no posiciona en buscadores, evitando que sus clientes lleguen a ella.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-do active text-center mb-30">
                            <div class="do-icon">
                                <span class="flaticon-vasos"></span>
                            </div>
                            <div class="do-caption">
                                <h4>No tienen Visibilidad</h4>
                                <p>No generan esa potencia en los canales con sus usuarios y así optimizar sus ventas o visibilidad.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- What We do End-->

        <!-- We Create Start -->
        <div class="we-create-area visite-padding2">
            <div class="container">
                <div class="row d-flex align-items-end">
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-cap">
                            <h3>¿Por qué elegirnos sobre nuestros competidores?</h3>
                            <p>Somos un grupo de profesionales multidisciplinarios en tecnologías digitales. <br><br>
                            Buscamos que cada proyecto en el que trabajamos sea único y personalizado aportando resultados potentes a nuestros
                            clientes y que puedan generar oportunidades de negocio.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-img mb-30">
                            <img src="{{URL::asset('assets_web/img//about/team.svg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- We Create End -->

        @include('layouts.cta')   

    </main>

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
