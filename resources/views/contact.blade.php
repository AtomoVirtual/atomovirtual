<!doctype html>
<html lang="es">
@include('layouts.head')
<body>
@include('layouts.loader')
@include('layouts.header')

<main>
    <!-- Slider Area Start-->
    <div class="services-area">
        <div class="container">
            <!-- Section-tittle -->
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8">
                    <div class="section-tittle text-center mb-80">
                        <span>Contacto</span>
                        <h2>Contáctanos​</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Slider Area End-->

    <!-- ================ contact section start ================= -->
    <section class="contact-section ">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Estamos a un Toque</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact" action="{{route('home.contact.store')}}" method="POST"  >
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Asunto">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mensaje'" placeholder=" Enter Message"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Nombre">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="lastname" id="lastname" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your lastname'" placeholder="Apellido">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="phone" id="phone" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter phone '" placeholder="Teléfono">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-element-widget">
                                    <div class="switch-wrap d-flex justify-content-start">
                                    <div class="primary-checkbox mt-1 mr-3">
                                    <input type="checkbox" id="default-checkbox">
                                    <label for="default-checkbox"></label>
                                    </div>
                                    <a href="{{route('home.terms')}}"><p>Acepto los Términos y Condiciones</p></a>
                                    </div>
                                    </div>
                                </div>

                                <div class="col-sm-11">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Enviar</button>
                            </div>
                            <input type="hidden" name="hidden_email" value="0">
                        </form>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Barcelona, España.</h3>
                                <p>Ubícanos fácilmente</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <a href="https://api.whatsapp.com/send?phone=34610695232"><h3>+34 6106 95232</h3></a>
                                <p>Envíanos un WhatsApp</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <a href="mailto:contacto@atomotecnovirtual.com"><h3>contacto@atomotecnovirtual.com</h3></a>
                                <p>Escríbenos un Correo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- ================ contact section end ================= -->


<section class="choose-padding">
    @include('layouts.cta')
</section>


</main>

@include('layouts.footer')
@include('layouts.scripts')

</body>

</html>
